import java.awt.EventQueue;
import java.awt.FileDialog;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SpinnerNumberModel;

public class Frame {

	private JFrame frame;
	private static JProgressBar progressBar;
	private static JLabel lblProgress;
	private static JSpinner spinner;
	private JTextField textField;
	private JTextField textField_1;
	private JLabel lblTileSettings;
	private JLabel lblWidth;
	private JLabel lblHeight;
	private JLabel lblUpscale;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		try {
			UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
			UIManager.put("OptionPane.okButtonText", "OK, sorry :(");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Frame window = new Frame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws IOException 
	 */
	public Frame() throws IOException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws IOException 
	 */
	private void initialize() throws IOException {
		
		frame = new JFrame();
		frame.setTitle("Mosaic Generator - By Ciprian Anti");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("res/icon.png"));
		frame.setBounds(100, 100, 450, 385);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		JLabel lblInputImage = new JLabel("Input image:");
		lblInputImage.setBounds(12, 7, 426, 20);
		frame.getContentPane().add(lblInputImage);
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBounds(12, 39, 300, 32);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Browse");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				FilenameFilter imageFilter = new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						// TODO Auto-generated method stub
						return name.endsWith(".png") || name.endsWith(".jpg") || name.endsWith(".jpeg");
					}
				};
				
				FileDialog fileDialog = new FileDialog(frame, "Choose image model", FileDialog.LOAD);
				fileDialog.setFile("Image");
				fileDialog.setFilenameFilter(imageFilter);
				fileDialog.setVisible(true);
				if (fileDialog.getFile() != null) {
					textField.setText(fileDialog.getDirectory() + fileDialog.getFile());
				}
			}
		});
		btnNewButton.setBounds(321, 39, 117, 32);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblInputTilesFolder = new JLabel("Input tiles folder:");
		lblInputTilesFolder.setBounds(12, 78, 426, 20);
		frame.getContentPane().add(lblInputTilesFolder);
		
		textField_1 = new JTextField();
		textField_1.setEditable(false);
		textField_1.setBounds(12, 110, 300, 32);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("Browse");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {			
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = fileChooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					textField_1.setText(fileChooser.getSelectedFile().getAbsolutePath());
				}
			}
		});
		btnNewButton_1.setBounds(321, 110, 117, 32);
		frame.getContentPane().add(btnNewButton_1);
		
		progressBar = new JProgressBar();
		progressBar.setBounds(12, 337, 426, 8);
		frame.getContentPane().add(progressBar);
		
		JButton btnGo = new JButton("GIMME THEM MOSAICS");
		btnGo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (textField.getText().isEmpty() || textField_1.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "OMG input some images u dumbass <_<", "Nope", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						try {
							Main.start(textField.getText(), textField_1.getText(), Integer.parseInt(textField_2.getText()), 
									Integer.parseInt(textField_3.getText()), Integer.parseInt(spinner.getValue().toString()));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							JOptionPane.showMessageDialog(null, "Nice job genius, you just crashed the program... here's the error: " + e.getMessage(), "Error <_<", JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						}
					}
				}).start();
					
				
			}
		});
		btnGo.setBounds(12, 230, 426, 68);
		frame.getContentPane().add(btnGo);
		
		lblProgress = new JLabel("Progress");
		lblProgress.setBounds(12, 310, 426, 15);
		frame.getContentPane().add(lblProgress);
		
		lblTileSettings = new JLabel("Tile settings:");
		lblTileSettings.setBounds(12, 154, 426, 20);
		frame.getContentPane().add(lblTileSettings);
		
		JPanel panel = new JPanel();
		panel.setBounds(12, 186, 140, 32);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		lblWidth = new JLabel("Width:");
		lblWidth.setHorizontalAlignment(SwingConstants.CENTER);
		lblWidth.setBounds(0, 0, 71, 32);
		panel.add(lblWidth);
		
		textField_2 = new JTextField();
		textField_2.setBounds(71, 0, 69, 32);
		panel.add(textField_2);
		textField_2.setText("32");
		textField_2.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(155, 186, 140, 32);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		lblHeight = new JLabel("Height:");
		lblHeight.setHorizontalAlignment(SwingConstants.CENTER);
		lblHeight.setBounds(0, 0, 71, 32);
		panel_1.add(lblHeight);
		
		textField_3 = new JTextField();
		textField_3.setBounds(71, 0, 69, 32);
		panel_1.add(textField_3);
		textField_3.setText("32");
		textField_3.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(296, 186, 140, 32);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		lblUpscale = new JLabel("Upscale:");
		lblUpscale.setHorizontalAlignment(SwingConstants.CENTER);
		lblUpscale.setBounds(0, 0, 71, 32);
		panel_2.add(lblUpscale);
		
		spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(1, 1, 4, 1));
		spinner.setBounds(71, 0, 69, 32);
		panel_2.add(spinner);
	}
	
	public static void updateProgress(int percent){
		progressBar.setValue(percent);
	}
	
	public static void updateProgressMsg(String msg){
		lblProgress.setText(msg);
	}
	
	public static void setProgressMax(int max){
		progressBar.setMaximum(max);;
	}
}
