import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.imageio.ImageIO;

import org.omg.CosNaming._BindingIteratorImplBase;

public class Main {
	private static String TILES_DIR = "/home/ciprian/Desktop/pic";
	private static String INPUT_IMG = "/home/ciprian/Desktop/i.png";
	private static String OUTPUT_IMG = "/home/ciprian/Desktop/o.png";
	private static int TILE_WIDTH = 0;
	private static int TILE_HEIGHT = 0;
	private static int TILE_UP_SCALE = 1;
	private static final boolean IS_BW = false;
	private static final int THREADS = 4;
	
	private static int loadingCounter = 1;

	private static void log(String msg) {
		System.out.println(msg);
	}

	public static void start(String inputImg, String tilesDir, int tileWidth, int tileHeight, int upscale) 
			throws IOException, InterruptedException {
		
		INPUT_IMG = inputImg;
		TILE_WIDTH = tileWidth;
		TILE_HEIGHT = tileHeight;
		TILE_UP_SCALE = upscale;
		
		System.out.println("w:" + TILE_WIDTH + " h:" + TILE_HEIGHT + " S:" + TILE_UP_SCALE);
		
		long startTime = System.nanoTime();
		log("Reading tiles...");
		final Collection<Tile> tileImages = getImagesFromTiles(new File(tilesDir));

		log("Processing input image...");
		File inputImageFile = new File(inputImg);
		Collection<BufferedImagePart> inputImageParts = getImagesFromInput(inputImageFile);
		final Collection<BufferedImagePart> outputImageParts = Collections
				.synchronizedSet(new HashSet<BufferedImagePart>());

		ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(THREADS);

		final AtomicInteger i = new AtomicInteger();
		final int partCount = inputImageParts.size();
		Frame.setProgressMax(partCount);
		for (final BufferedImagePart inputImagePart : inputImageParts) {
			newFixedThreadPool.execute(new Runnable() {
				public void run() {
					Tile bestFitTile = getBestFitTile(inputImagePart.image, tileImages);
					log(String.format("Matching part %s of %s", i.incrementAndGet(), partCount));
					Frame.updateProgress(i.get());
					Frame.updateProgressMsg("Matching best tiles...");
					outputImageParts.add(new BufferedImagePart(bestFitTile.image, inputImagePart.x, inputImagePart.y));
				}
			});
		}

		newFixedThreadPool.shutdown();
		newFixedThreadPool.awaitTermination(10000000, TimeUnit.SECONDS);

		log("Writing output image...");
		BufferedImage inputImage = ImageIO.read(inputImageFile);
		int width = inputImage.getWidth();
		int height = inputImage.getHeight();
		BufferedImage output = makeOutputImage(width, height, outputImageParts);
		log("Saving image...");
		ImageIO.write(output, "png", new File(INPUT_IMG.substring(0, INPUT_IMG.lastIndexOf(".")) +
				"_" + TILE_WIDTH + "x" + TILE_HEIGHT + "_mosaiq.png"));
		log("FINISHED");
		Frame.setProgressMax(100);
		Frame.updateProgress(100);
		Frame.updateProgressMsg("Done.");
		long endTime = System.nanoTime();
		log("Duration: " + ((endTime - startTime) / 1000000000) + " s");
	}

	private static BufferedImage makeOutputImage(int width, int height, Collection<BufferedImagePart> parts) throws InterruptedException {
		BufferedImage image = new BufferedImage(width * TILE_UP_SCALE, height * TILE_UP_SCALE, BufferedImage.TYPE_3BYTE_BGR);
		
		int last = parts.size();
		Frame.updateProgressMsg("Generating the bigger image...");
		Frame.setProgressMax(last);
		
		ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(THREADS);
		
		for (BufferedImagePart part : parts) {
			newFixedThreadPool.execute(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					BufferedImage imagePart = image.getSubimage(part.x * TILE_UP_SCALE, part.y * TILE_UP_SCALE, TILE_WIDTH,
							TILE_HEIGHT);
					imagePart.setData(part.image.getData());
					log(String.format("Part %s of %s", loadingCounter, last));
					loadingCounter++;
					Frame.updateProgress(loadingCounter);
				}
			});
		}
		
		loadingCounter = 0;
		
		newFixedThreadPool.shutdown();
		newFixedThreadPool.awaitTermination(10000000, TimeUnit.SECONDS);

		return image;
	}

	private static Tile getBestFitTile(BufferedImage target, Collection<Tile> tiles) {
		Tile bestFit = null;
		int bestFitScore = -1;

		for (Tile tile : tiles) {
			int score = getScore(target, tile);
			if (score > bestFitScore) {
				bestFitScore = score;
				bestFit = tile;
			}
		}

		return bestFit;
	}

	private static int getScore(BufferedImage target, Tile tile) {
		assert target.getHeight() == Tile.scaledHeight;
		assert target.getWidth() == Tile.scaledWidth;

		int total = 0;
		for (int x = 0; x < Tile.scaledWidth; x++) {
			for (int y = 0; y < Tile.scaledHeight; y++) {
				int targetPixel = target.getRGB(x, y);
				Pixel candidatePixel = tile.pixels[x][y];
				int diff = getDiff(targetPixel, candidatePixel);
				int score;
				if (IS_BW) {
					score = 255 - diff;
				} else {
					score = 255 * 3 - diff;
				}

				total += score;
			}
		}

		return total;
	}

	private static int getDiff(int target, Pixel candidate) {
		if (IS_BW) {
			return Math.abs(getRed(target) - candidate.r);
		} else {
			return Math.abs(getRed(target) - candidate.r) + Math.abs(getGreen(target) - candidate.g)
					+ Math.abs(getBlue(target) - candidate.b);
		}
	}

	private static int getRed(int pixel) {
		return (pixel >>> 16) & 0xff;
	}

	private static int getGreen(int pixel) {
		return (pixel >>> 8) & 0xff;
	}

	private static int getBlue(int pixel) {
		return pixel & 0xff;
	}

	private static Collection<Tile> getImagesFromTiles(File tilesDir) throws IOException {
		Collection<Tile> tileImages = Collections.synchronizedSet(new HashSet<Tile>());
		File[] files = tilesDir.listFiles();
		for (File file : files) {
			BufferedImage img = ImageIO.read(file);
			if (img != null) {
				tileImages.add(new Tile(img));
			} else {
				System.err.println("null image for file " + file.getName());
			}
		}
		return tileImages;
	}

	private static Collection<BufferedImagePart> getImagesFromInput(File inputImgFile) throws IOException {
		Collection<BufferedImagePart> parts = new HashSet<BufferedImagePart>();

		BufferedImage inputImage = ImageIO.read(inputImgFile);
		int totalHeight = inputImage.getHeight();
		int totalWidth = inputImage.getWidth();

		int x = 0, y = 0, w = Tile.scaledWidth, h = Tile.scaledHeight;
		while (x + w <= totalWidth) {
			while (y + h <= totalHeight) {
				BufferedImage inputImagePart = inputImage.getSubimage(x, y, w, h);
				parts.add(new BufferedImagePart(inputImagePart, x, y));
				y += h;
			}
			y = 0;
			x += w;
		}

		return parts;
	}

	public static class Tile {
		public static int scaledWidth;
		public static int scaledHeight;
		public Pixel[][] pixels;
		public BufferedImage image;

		public Tile(BufferedImage image) {
			scaledWidth = TILE_WIDTH / TILE_UP_SCALE;
			scaledHeight = TILE_HEIGHT / TILE_UP_SCALE;
			pixels = new Pixel[scaledWidth][scaledHeight];
			this.image = image;
			calcPixels();
		}

		private void calcPixels() {
			for (int x = 0; x < scaledWidth; x++) {
				for (int y = 0; y < scaledHeight; y++) {
					pixels[x][y] = calcPixel(x * TILE_UP_SCALE, y * TILE_UP_SCALE, TILE_UP_SCALE, TILE_UP_SCALE);
				}
			}
		}

		private Pixel calcPixel(int x, int y, int w, int h) {
			int redTotal = 0, greenTotal = 0, blueTotal = 0;

			for (int i = 0; i < w; i++) {
				for (int j = 0; j < h; j++) {
					int rgb = image.getRGB(x + i, y + j);
					redTotal += getRed(rgb);
					greenTotal += getGreen(rgb);
					blueTotal += getBlue(rgb);
				}
			}
			int count = w * h;
			return new Pixel(redTotal / count, greenTotal / count, blueTotal / count);
		}
	}

	public static class BufferedImagePart {
		public BufferedImagePart(BufferedImage image, int x, int y) {
			this.image = image;
			this.x = x;
			this.y = y;
		}

		public BufferedImage image;
		public int x;
		public int y;
	}

	public static class Pixel {
		public int r, g, b;

		public Pixel(int r, int g, int b) {
			this.r = r;
			this.g = g;
			this.b = b;
		}

		@Override
		public String toString() {
			return r + "." + g + "." + b;
		}
	}
}
